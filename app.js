require('./models/db')

const createError = require('http-errors')
const express = require('express')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const passport = require('passport')
const helmet = require('helmet')
const cors = require('cors')
const swaggerJsdoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const rateLimit = require('express-rate-limit')
const authRouter = require('./routes/auth.router')
const homeRouter = require('./routes/home.router')
const userRouter = require('./routes/user.router')
const test = require('./routes/test.router')
const calendarRouter = require('./routes/calendar.route')
const pharmacyRouter = require('./routes/pharmacy.router')
const ratingRouter = require('./routes/rating.router')
const recordRouter = require('./routes/medicalRecord.router')
const authenticate = require('./middleware/auth.middleware')
const User = require('./models/user.model')
const config = require('./config')
const payment = require('./middleware/payment.middleware')
const multer = require('multer')

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(helmet(config.helmetOptions))
app.use(rateLimit(config.rateLimit))
app.use(passport.initialize())

passport.use(authenticate.localStrategy)
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())
passport.use(authenticate.jwtStrategy)
passport.use('jwt-refresh', authenticate.jwtRefreshStrategy)
passport.use('jwt-admin', authenticate.jwtAdminStrategy)
// Cors for 3001 React port and credentials for cookies login
app.use(cors({ credentials: true, origin: 'http://localhost:3001' }))

// payment.createPlan();

app.use('/test', test)
app.use('/api/auth', authRouter)
app.use('/api/home', homeRouter)
app.use('/api/user', userRouter)
app.use('/api/pharmacy', pharmacyRouter)
app.use('/api/rating', ratingRouter)
app.use('/api/medicalRecord', recordRouter)
app.use('/api/', calendarRouter)

/*
app.use(
    ['/api/docs', '/api', '/'],
    swaggerUi.serve,
    swaggerUi.setup(swaggerJsdoc(config.swagger), {
        explorer: false,
        customSiteTitle: 'hop',
    }),
)
*/

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    // render the error page
    res.status(err.status || 500).send({ err })
})

module.exports = app
