const express = require('express')
const router = express.Router()
const authenticate = require('../middleware/auth.middleware')
const controller = require('../controllers/calendar.controller')

//router.post("/calendar/:practitionerId/practitionerAddPlanification", controller.practitionerAddPlanification);
//authenticate.verifyUserJwt: verify is login => apply to all routes
router.get(
    '/calendar/practionnerAllSlots',
    authenticate.verifyUserJwt,
    authenticate.isPractitioner,
    controller.practionnerAllSlots,
)
//router.post("/calendar/appointments/practitionner/:practitionerId/patient/:patientId", authenticate.isPractitioner, controller.appointments);
//, authenticate.isPractitionerOrPatient
//router.post("/calendar/appointments", controller.appointments);
router.post(
    '/calendar/appointments',
    authenticate.verifyUserJwt,
    authenticate.isPractitionerOrPatient,
    controller.appointments,
)
//, authenticate.isPractitionerOrPatient
router.post('/calendar/practitionerGetAvailableSlots', controller.practitionerGetAvailableSlots)
router.get(
    '/calendar/getAppointments',
    authenticate.verifyUserJwt,
    authenticate.isAuthorizedForAppointment,
    controller.getAppointments,
)
router.delete(
    '/calendar/delete-appointment/:appointmentId',
    authenticate.verifyUserJwt,
    authenticate.isAuthorizedForAppointment,
    controller.deleteAppointment,
)
router.put(
    '/calendar/update-appointment/:appointmentId',
    authenticate.verifyUserJwt,
    authenticate.isAuthorizedForAppointment,
    controller.updateAppointment,
)

module.exports = router
