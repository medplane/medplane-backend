const express = require('express')
const router = express.Router()
const Rating = require('../models/rating.model')
const authenticate = require('../middleware/auth.middleware')
const controller = require('../controllers/rating.controller')

router.post('/rate', authenticate.verifyUserJwt, authenticate.isPatient, controller.rateAndComment)

router.get('/', authenticate.verifyUserJwt, controller.getRatings)

router.delete(
    '/:id',
    authenticate.verifyUserJwt,
    authenticate.isAuthorizedToDeleteRating,
    controller.deleteRating,
)

module.exports = router
