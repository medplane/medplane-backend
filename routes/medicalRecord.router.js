const express = require('express')
const router = express.Router()
const authenticate = require('../middleware/auth.middleware')
const controller = require('../controllers/medicalRecord.controller')

// Ajouter un fichier au dossier médical
router.post('/', authenticate.verifyUserJwt, controller.addFile)

// Supprimer un fichier du dossier médical TODO
router.delete('/:id', authenticate.verifyUserJwt, controller.deleteFile)

// Recupere les fichiers du patient
router.get('/:id', authenticate.verifyUserJwt, controller.getFiles)

router.get('/:id', authenticate.verifyUserJwt, controller.getFile)

module.exports = router
