const express = require('express')
const router = express.Router()
// const body = require('express-validator').body
const controller = require('../controllers/home.controller')

router.post('/searchPractitioner', controller.searchPractitioner)

module.exports = router
