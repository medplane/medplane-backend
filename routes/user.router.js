const express = require('express')
const router = express.Router()
var auth = require('../middleware/auth.middleware')
const User = require('../models/user.model')
const Practitioner = require('../models/practitioner.model')
const Pharmacy = require('../models/pharmacy.model')
const Patient = require('../models/patient.model')

// get user profile
router.get('/', auth.verifyUserJwt, async function (req, res, next) {
    const userInfo = await User.findById(req.user._id).populate('roleInfoId')
    res.status(200)
    res.json({ user: userInfo })
})

router.get('/practitioner/:id',auth.verifyUserJwt,async function (req,res,next){
    const { id } = req.params;
    const userInfo = await Practitioner.findById(id).populate('userId')
    res.status(200)
    res.json({ user: userInfo })
})

router.get('/pharmacy/:id',auth.verifyUserJwt,async function (req,res,next){
    const { id } = req.params;
    const userInfo = await Pharmacy.findById(id).populate('userId')
    res.status(200)
    res.json({ user: userInfo })
})

router.get('/patient/:id',auth.verifyUserJwt,async function (req,res,next){
    const { id } = req.params;
    const userInfo = await Patient.findById(id).populate('userId')
    res.status(200)
    res.json({ user: userInfo })
})

router.get('/:id',auth.verifyUserJwt,async function (req,res,next){
    const { id } = req.params;
    const userInfo = await User.findById(id).populate('roleInfoId')
    res.status(200)
    res.json({ user: userInfo })
})

module.exports = router
