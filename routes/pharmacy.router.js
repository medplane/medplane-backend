const express = require('express')
const router = express.Router()
const controller = require('../controllers/pharmacy.controller')
const authenticate = require('../middleware/auth.middleware')

router.post('/requestMedicine', authenticate.verifyUserJwt, controller.requestMedicine)

router.post('/searchPharmacy', controller.searchPharmacy)

router.get('/medicineRequests', authenticate.verifyUserJwt, controller.getRequests)

router.put(
    '/medicineRequests/:id',
    authenticate.verifyUserJwt,
    authenticate.isAuthorizedForRequestEdit,
    controller.editRequest,
)

router.delete(
    '/medicineRequests/:id',
    authenticate.verifyUserJwt,
    authenticate.isAuthorizedToDeleteRequest,
    controller.deleteRequest,
)

module.exports = router
