const axios = require('axios')
const geolib = require('geolib')

const middleware = {
    getRegion: async function (address) {
        const response = await axios.get('https://nominatim.openstreetmap.org/search', {
            params: {
                q: address,
                format: 'json',
                limit: 1,
            },
        })

        console.log('data output = ', response.data)
        const lat = response.data[0].lat
        const lon = response.data[0].lon

        const region = geolib.getRegion({
            latitude: lat,
            longitude: lon,
        })
        // Récupérez la région (par exemple, la ville) à partir de la réponse de géocodage
        // const region = response.data[0].address.city;

        return region
    },
}

module.exports = middleware
