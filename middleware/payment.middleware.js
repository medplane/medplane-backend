const config = require('../config')
const stripe = require('stripe')(config.stripe.secret_key)

exports.createPlan = () => {
    stripe.plans.create(
        {
            amount: 999, //9.99 e
            currency: 'eur',
            interval: 'month',
            product: {
                name: 'Abonnement mensuel',
            },
            id: 'medplane_practitioner_subscription',
        },
        function (err, plan) {
            if (err) {
                // Gérer les erreurs
            } else {
                // Le plan d'abonnement a été créé avec succès
                console.log(plan)
            }
        },
    )
}

exports.generateCardToken = async function (cardNumber, expMonth, expYear, cvc) {
    try {
        const token = await stripe.tokens.create({
            card: {
                number: cardNumber,
                exp_month: expMonth,
                exp_year: expYear,
                cvc: cvc,
            },
        })
        return token.id
    } catch (error) {
        console.error(error)
        throw new Error('Erreur lors de la génération du token de carte')
    }
}
