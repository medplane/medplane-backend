const passport = require('passport')
const jwt = require('jsonwebtoken')
const LocalStrategy = require('passport-local').Strategy
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const User = require('../models/user.model')
const config = require('../config')
const day = require('dayjs')
const MedicineRequest = require('../models/medicineRequest.model')
const Pharmacy = require('../models/pharmacy.model')
const Rating = require('../models/rating.model')

const { accessTokenPrivateKey, refreshTokenPrivateKey, resetTokenPrivateKey, expireIn } =
    config.auth
const roles = config.roles

const verify = (jwt_payload, done) => {
    User.findOne({ _id: jwt_payload._id, revoked: null }, (err, user) => {
        if (err) {
            return done(err, false)
        } else if (user) {
            return done(null, user)
        } else {
            return done(null, false)
        }
    })
}

const verifyAdmin = (jwt_payload, done) => {
    User.findOne({ _id: jwt_payload._id, roles: roles.admin, revoked: null }, (err, user) => {
        if (err) {
            return done(err, false)
        } else if (user) {
            return done(null, user)
        } else {
            return done(null, false)
        }
    })
}

exports.jwtAccessTokenCookieExtractor = (req) => {
    let accessToken = null
    if (req && req.cookies) {
        accessToken = req.cookies['accessToken']
    }
    return accessToken
}

exports.jwtRefreshTokenCookieExtractor = (req) => {
    let refreshToken = null
    if (req && req.cookies) {
        refreshToken = req.cookies['refreshToken']
    }
    return refreshToken
}

exports.getAccessToken = (user) => {
    return jwt.sign(user, accessTokenPrivateKey, { expiresIn: expireIn.accessToken })
}

exports.getRefreshToken = (user) => {
    return jwt.sign(user, refreshTokenPrivateKey, { expiresIn: expireIn.refreshToken })
}

exports.getResetToken = (user) => {
    return jwt.sign(user, resetTokenPrivateKey, { expiresIn: expireIn.resetToken })
}

exports.localStrategy = new LocalStrategy(User.authenticate())

exports.jwtStrategy = new JwtStrategy(
    {
        jwtFromRequest: ExtractJwt.fromExtractors([exports.jwtAccessTokenCookieExtractor]),
        secretOrKey: accessTokenPrivateKey,
    },
    verify,
)

exports.jwtRefreshStrategy = new JwtStrategy(
    {
        jwtFromRequest: ExtractJwt.fromExtractors([exports.jwtRefreshTokenCookieExtractor]),
        secretOrKey: refreshTokenPrivateKey,
    },
    verify,
)

exports.jwtAdminStrategy = new JwtStrategy(
    {
        jwtFromRequest: ExtractJwt.fromExtractors([exports.jwtAccessTokenCookieExtractor]),
        secretOrKey: accessTokenPrivateKey,
    },
    verifyAdmin,
)

exports.isPractitioner = (req, res, next) => {
    if (req.user.roles === 'Practitioner') {
        return next()
    } else {
        //Unauthorized: you are not "Practitioner" or "Patient
        return res.status(401).send('Unauthorized: you are not Practitioner')
    }
}

exports.isPatient = (req, res, next) => {
    if (req.user.roles === 'Patient') {
        return next()
    } else {
        return res.status(401).send('Unauthorized: you are not a Patient')
    }
}

exports.isPractitionerOrPatient = (req, res, next) => {
    if (req.user.roles === 'Practitioner' || req.user.roles === 'Patient') {
        return next()
    } else {
        return res.status(401).send('Unauthorized: you are not Practitioner or Patient')
    }
}

exports.isAuthorizedForAppointment = (req, res, next) => {
    if (req.user.roles === 'Practitioner' || req.user.roles === 'Patient') {
        return next()
    } else {
        return res.status(401).send('Unauthorized: you are not authorized consulting appointment')
    }
}

exports.isAuthorizedForRequestEdit = async (req, res, next) => {
    const { id } = req.params

    const medicineRequest = await MedicineRequest.findById(id)
    if (!medicineRequest) {
        return res.status(404).json({ message: 'Demande de médicament introuvable' })
    }

    if (req.user.roleInfoId._id.equals(medicineRequest.pharmacyId) || req.user.roles === 'Admin') {
        return next()
    } else {
        return res
            .status(403)
            .json({ message: 'Unauthorized: you are not authorized to edit the request' })
    }
}

exports.isAuthorizedToDeleteRating = async (req, res, next) => {
    const { id } = req.params
    const userId = req.user._id

    try {
        const rating = await Rating.findById(id)

        if (!rating) {
            return res.status(404).json({ message: 'Rating not found' })
        }

        if (rating.ratedBy.toString() !== userId.toString() && req.user.roles !== 'Admin') {
            return res
                .status(403)
                .json({ message: 'Unauthorized: You are not allowed to delete this rating' })
        }

        next()
    } catch (err) {
        next(err)
    }
}

exports.isAuthorizedToDeleteRequest = async (req, res, next) => {
    const { id } = req.params
    const userId = req.user._id

    try {
        const medicineRequest = await MedicineRequest.findById(id)

        if (!medicineRequest) {
            return res.status(404).json({ message: 'Medicine request not found' })
        }

        if (
            medicineRequest.askerId.toString() !== userId.toString() &&
            req.user.roles !== 'Admin'
        ) {
            return res
                .status(403)
                .json({
                    message: 'Unauthorized: You are not allowed to delete this medicine request',
                })
        }

        next()
    } catch (err) {
        next(err)
    }
}

exports.verifyUserLocal = passport.authenticate('local', { session: false })
exports.verifyUserJwt = passport.authenticate('jwt', { session: false })
exports.verifyUserJwtRefresh = passport.authenticate('jwt-refresh', { session: false })
exports.verifyUserAdminJwt = passport.authenticate('jwt-admin', { session: false })
