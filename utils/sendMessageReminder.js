const Appointment = require('../models/appointment.model')
const sendEmail = require('../utils/sendEmail')

const sendMessageReminder = async () => {
    try {
        const maintenant = new Date()
        const heureRdvMoins24h = new Date(maintenant.getTime() + 24 * 60 * 60 * 1000)

        //console.log("maintenant: "+maintenant +"    heureRdvMoins24h :"+heureRdvMoins24h);
        const rdvs24h = await Appointment.find({
            date: {
                $gte: maintenant,
                $lt: heureRdvMoins24h,
            },
            reminderSent24h: false,
        })
            .populate({
                path: 'patientId',
                populate: {
                    path: 'userId',
                },
            })
            .populate({
                path: 'practitionerId',
                populate: {
                    path: 'userId',
                },
            })

        console.log('rdvs24h :' + rdvs24h)

        // Envoi des messages de rappel aux rendez-vous 24h avant
        for (const rdv of rdvs24h) {
            // Marquer le rendez-vous comme ayant reçu le rappel
            rdv.reminderSent24h = true
            await rdv.save()

            //Envoie d'Email pour le rdv au patient
            const subject = `Rappel de RDV dans aujaurd'hui à ${rdv.time}`
            const message24h = `<p>Bonjour ${rdv.patientId.firstName},</p>
            <p>Ceci est un rappel pour votre rendez-vous aujaurd'hui à ${rdv.time}.</p>`

            await sendEmail(rdv.patientId.userId.email, subject, message24h)
        }
    } catch (error) {
        console.log(error, 'email not sent')
    }
}

module.exports = sendMessageReminder
