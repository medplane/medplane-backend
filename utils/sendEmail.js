const nodemailer = require('nodemailer')
const path = require('path')

const sendEmail = async (email, subject, text) => {
    // Configurer les informations d'authentification
    const logoWidth = 400
    try {
        const transporter = nodemailer.createTransport({
            host: process.env.HOST,
            service: process.env.EMAIL_SERVICE,
            port: 587,
            secure: true,
            auth: {
                user: process.env.EMAIL_USER,
                pass: process.env.EMAIL_PASS,
            },
        })

        const formattedText = text.replace(/\n/g, '<br>')
        const htmlContent = `
                            <p>${formattedText}</p>
                            <img src="cid:logo" alt="Logo" width="${logoWidth}" />
                        `

        await transporter.sendMail({
            from: {
                name: 'Medplane',
                address: process.env.EMAIL_USER_HEADER,
            },
            to: [email], //list of receivers
            subject: subject,
            html: htmlContent,

            attachments: [
                {
                    filename: 'banner.jpg',
                    path: path.join(__dirname, '../public/banner.jpg'),
                    //contentType: 'application/sjpg',
                    cid: 'logo',
                },
            ],
        })

        console.log('email sent sucessfully')
    } catch (error) {
        console.log(error, 'email not sent')
    }
}

module.exports = sendEmail
