// utils/utils.js

// Comparaison de deux dates en utilisant l'année, le mois et le jour
function compareDates(date1, date2) {
    const year1 = date1.getFullYear()
    const month1 = date1.getMonth()
    const day1 = date1.getDate()

    const year2 = date2.getFullYear()
    const month2 = date2.getMonth()
    const day2 = date2.getDate()

    if (year1 === year2 && month1 === month2 && day1 === day2) {
        return 0 // Les dates sont identiques
    } else if (date1 < date2) {
        return -1 // La date1 est antérieure à date2
    } else {
        return 1 // La date1 est postérieure à date2
    }
}

// Vérifie le format de la date (année-mois-jour : YYYY-MM-DD ou YYYY/MM/DD)
function isValidDateFormatYMD(dateString) {
    // Expression régulière pour les formats YYYY-MM-DD et YYYY/MM/DD
    const dateFormatRegex = /^(\d{4})[-/](\d{2})[-/](\d{2})$/

    // Vérification du format de la date
    return dateFormatRegex.test(dateString)
}

function getStartAndEndOfDay(date) {
    const startOfDay = new Date(date.getFullYear(), date.getMonth(), date.getDate())
    const endOfDay = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1)

    return { startOfDay, endOfDay }
}

function isWithin24Hours(date) {
    const twentyFourHoursInMilliseconds = 24 * 60 * 60 * 1000
    const appointmentDate = new Date(date)
    const currentDateTime = new Date()

    return appointmentDate.getTime() - currentDateTime.getTime() > twentyFourHoursInMilliseconds
}

// Export de la fonction compareDates
module.exports = {
    compareDates,
    isValidDateFormatYMD,
    getStartAndEndOfDay,
    isWithin24Hours,
}
