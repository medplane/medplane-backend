const { storage } = require('../firebase.js')
const MedicalRecord = require('../models/medicalRecord.model.js')
const Patient = require('../models/patient.model.js')
const multer = require('multer')
const upload = multer().single('file')

module.exports = {
    addFile: async function (req, res, next) {
        try {
            // const patientId = req.body.patientId;
            const patientId = '64899263f2a403cb5eb3dec1'

            const userId = req.user._id

            if (
                req.user.roleInfoId._id.toString() !== patientId.toString() &&
                req.user.roles !== 'Practitioner'
            ) {
                return res.status(403).json({ message: 'Unothorized !' })
            }

            upload(req, res, async (err) => {
                if (err) {
                    console.error("Erreur lors de l'upload du fichier :", err)
                    return res.status(500).json({ message: "Erreur lors de l'upload du fichier" })
                }

                const file = req.file

                // Vérifier si un fichier a été envoyé
                if (!file) {
                    return res.status(400).json({ message: 'Aucun fichier trouvé dans la requête' })
                }

                const storageRef = storage.ref()

                let fileName = file.originalname

                if (req.body.fileName) {
                    fileName = req.body.fileName
                }
                const fileRef = storageRef.child(`medical-records/${patientId}/${fileName}`)
                const uploadTask = fileRef.put(file.buffer)

                uploadTask.on(
                    'state_changed',
                    (snapshot) => {
                        // Gestion des états de progression de l'upload
                    },
                    (error) => {
                        console.error("Erreur lors de l'upload du fichier :", error)
                        return res
                            .status(500)
                            .json({ message: "Erreur lors de l'upload du fichier" })
                    },
                    async () => {
                        // Enregistrer les informations du fichier dans la base de données
                        const fileUrl = await fileRef.getDownloadURL()

                        const newFile = {
                            patientId: patientId,
                            userId: userId,
                            fileName: fileName,
                            fileUrl: fileUrl,
                            uploadedDate: new Date(),
                        }

                        const savedFile = await MedicalRecord.create(newFile)

                        return res
                            .status(200)
                            .json({ message: 'Fichier ajouté avec succès', file: savedFile })
                    },
                )
            })
        } catch (err) {
            console.error("Erreur lors de l'ajout du fichier :", err)
            next(err)
        }
    },

    deleteFile: async function (req, res, next) {
        try {
            // const fileId = req.body.fileName;
            const fileId = req.params.id

            const file = await MedicalRecord.findById(fileId)
            if (!file) {
                return res.status(404).json({ message: 'Fichier introuvable' })
            }

            // Vérifier si l'utilisateur est autorisé à supprimer le fichier du dossier médical
            if (!req.user._id.equals(file.userId)) {
                return res.status(403).json({ message: 'Unothorized !' })
            }

            const storageRef = storage.refFromURL(file.fileUrl)
            await storageRef.delete()

            await file.remove()

            return res.status(200).json({ message: 'Fichier supprimé avec succès' })
        } catch (err) {
            console.error('Erreur lors de la suppression du fichier :', err)
            next(err)
        }
    },

    getFiles: async function (req, res, next) {
        try {
            const patientId = req.params.id

            const patient = await Patient.findById(patientId)

            if (!patient) {
                return res.status(404).json({ message: 'Patient introuvable' })
            }

            if (
                req.user.roleInfoId._id.toString() !== patientId.toString() &&
                req.user.roles !== 'Practitioner'
            ) {
                return res.status(403).json({ message: 'Unothorized !' })
            }

            const files = await MedicalRecord.find({ patientId: patientId })
            return res.status(200).json(files)
        } catch (err) {
            next(err)
        }
    },

    getFile: async function (req, res, next) {
        try {
            const fileId = req.params.id

            const file = await MedicalRecord.findById(fileId)
            if (!file) {
                return res.status(404).json({ message: 'Fichier introuvable' })
            }

            if (
                req.user.roleInfoId._id.toString() !== file.patientId.toString() &&
                req.user.roles !== 'Practitioner'
            ) {
                return res.status(403).json({ message: 'Unothorized !' })
            }

            res.status(200).json({ message: 'Recherche effectuée avec succés', fileUrl: file })
        } catch (err) {
            next(err)
        }
    },
}
