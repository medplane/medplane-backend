const MedicineRequest = require('../models/medicineRequest.model')
const Pharmacy = require('../models/pharmacy.model')
const User = require('../models/user.model')
const natural = require('natural')

const controller = {
    searchPharmacy: async function (req, res, next) {
        try {
            const pharmacies = await Pharmacy.find().populate({ path: 'userId' })

            // Computing scores for each pharmacy to sort them in the result
            const scoredPharmacies = pharmacies.map((pharmacy) => {
                let score = 1

                if (req.body.city) {
                    var distance = natural.LevenshteinDistance(req.body.city, pharmacy.userId.city)
                    var similarite =
                        1 - distance / Math.max(req.body.city.length, pharmacy.userId.city.length)
                    score += similarite * 5

                    distance = natural.LevenshteinDistance(req.body.city, pharmacy.name)
                    similarite = 1 - distance / Math.max(req.body.city.length, pharmacy.name.length)
                    score += similarite * 10
                }

                return {
                    pharmacy,
                    score,
                }
            })

            // Sorting the practitioners according to the scores
            const sortedPharmacies = scoredPharmacies.sort((a, b) => b.score - a.score)

            res.status(200).json({
                message: 'Recherche effectuée avec succès.',
                pharmacies: sortedPharmacies,
            })
        } catch (error) {
            // Gérez les erreurs de manière appropriée
            console.error('Erreur lors de la recherche de pharmaciens :', error)
            return { error: "Une erreur s'est produite lors de la recherche de pharmacies." }
        }
    },

    requestMedicine: async function (req, res, next) {
        try {
            const { pharmacyId, medicine, comment } = req.body
            const askerId = req.user._id

            const pharmacy = await Pharmacy.findById(pharmacyId)

            if (!pharmacy) {
                return res.status(404).json({ message: 'Pharmacie introuvable' })
            }

            const asker = await User.findById(askerId)
            if (!asker) {
                return res.status(404).json({ message: 'User introuvable' })
            }

            const currentDate = new Date()

            const request = new MedicineRequest({
                date: currentDate,
                askerId: askerId,
                askerRole: asker.roles,
                pharmacyId: pharmacyId,
                medicine: medicine,
                comments: [
                    {
                        commentedBy: askerId,
                        date: currentDate,
                        message: comment,
                    },
                ],
                updatedAt: currentDate,
            })

            // saving medicineRequest
            await request.save()

            return res.status(200).json({ message: 'Demande créée avec succès' })
        } catch (err) {
            next(err)
        }
    },

    getRequests: async function (req, res, next) {
        try {
            const requestsCreated = await MedicineRequest.find({ askerId: req.user._id })
            let requestsReceived

            if ((req.user.roles = 'Pharmacy')) {
                requestsReceived = await MedicineRequest.find({ pharmacyId: req.user.roleInfoId })
            }

            res.status(200).json({
                requestsCreated: requestsCreated,
                requestsReceived: requestsReceived,
            })
        } catch (error) {
            next(error)
        }
    },

    editRequest: async function (req, res, next) {
        try {
            const { id } = req.params
            const { status, comment } = req.body

            if (status !== 'Disponible' && status !== 'Indisponible' && status !== 'N/A') {
                return res
                    .status(401)
                    .json({ message: 'Unauthorized: Status must be : disponible or indisponible' })
            }

            // Vérifier si la demande de médicament existe
            const medicineRequest = await MedicineRequest.findById(id)
            const currentDate = new Date()

            medicineRequest.status = status
            medicineRequest.comments.push({
                commentedBy: req.user._id,
                date: currentDate,
                message: comment,
            })
            medicineRequest.updatedAt = new Date()

            await medicineRequest.save()

            return res
                .status(200)
                .json({ message: 'Statut de la demande de médicament mis à jour avec succès' })
        } catch (err) {
            next(err)
        }
    },

    deleteRequest: async function (req, res, next) {
        const { id } = req.params

        try {
            const medicineRequest = await MedicineRequest.findById(id)

            if (!medicineRequest) {
                return res.status(404).json({ message: 'Medicine request not found' })
            }

            await medicineRequest.remove()

            return res.status(200).json({ message: 'Medicine request deleted successfully' })
        } catch (err) {
            next(err)
        }
    },
}

module.exports = controller
