const { param } = require('../app')
const Rating = require('../models/rating.model')
const User = require('../models/user.model')

const controller = {
    rateAndComment: async function (req, res, next) {
        try {
            const { ratedEntityId, rating, comment } = req.body
            const ratedBy = req.user._id

            const user = await User.findOne({ roleInfoId: ratedEntityId }).select('roles')

            if (!user) {
                return res.status(404).json({ message: 'Professionnel introuvable' })
            }

            const toSave = new Rating({
                ratedBy: ratedBy,
                ratedEntity: ratedEntityId,
                entityType: user.roles,
                rating: rating,
                comment: comment,
            })

            await toSave.save()

            return res.status(200).json({ message: 'Évaluation enregistrée avec succès' })
        } catch (err) {
            next(err)
        }
    },

    getRatings: async function (req, res, next) {
        try {
            const user = await User.findById(req.user._id)

            const ratings = await Rating.find({ ratedEntity: user.roleInfoId }).select(
                'rating comment',
            )

            const totalRatings = ratings.length

            // Compute the sum of all ratings
            const ratingSum = ratings.reduce((sum, rating) => sum + rating.rating, 0)

            // Compute the average rating
            const averageRating = totalRatings > 0 ? ratingSum / totalRatings : 0

            return res.status(200).json({
                average: averageRating,
                list: ratings,
            })
        } catch (err) {
            next(err)
        }
    },

    deleteRating: async function (req, res, next) {
        const { id } = req.params

        try {
            const rating = await Rating.findById(id)

            // Check if the rating exists
            if (!rating) {
                return res.status(404).json({ message: 'Rating not found' })
            }

            // Delete the rating
            await rating.remove()

            return res.status(200).json({ message: 'Rating deleted successfully' })
        } catch (err) {
            next(err)
        }
    },
}

module.exports = controller
