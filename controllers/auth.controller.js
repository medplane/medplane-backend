const passport = require('passport')
const validationResult = require('express-validator').validationResult
const authenticate = require('../middleware/auth.middleware')
const User = require('../models/user.model')
const Patient = require('../models/patient.model')
const RefreshToken = require('../models/refreshToken.model')
const config = require('../config')
const Practitioner = require('../models/practitioner.model')
const MedicalInstitute = require('../models/medicalInstitute.model')
const day = require('dayjs')
const middleware = require('../middleware/home.middleware')
const paymentMiddleware = require('../middleware/payment.middleware')
const Pharmacy = require('../models/pharmacy.model')
const stripe = require('stripe')(config.stripe.secret_key)
const sendEmail = require('../utils/sendEmail')
const jwt = require('jsonwebtoken')
const ResetToken = require('../models/resetToken.model')
const { accessTokenPrivateKey, refreshTokenPrivateKey, expireIn } = config.auth
const sendMessageReminder = require('../utils/sendMessageReminder')

const controller = {
    login: async function (req, res, next) {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    success: false,
                    errors: errors.array(),
                })
            }

            if (req.user.revoked) {
                res.status(401).json({
                    success: false,
                    message: 'This account is banned!',
                })
                return
            }

            const accessToken = authenticate.getAccessToken({ _id: req.user._id })
            const refreshToken = authenticate.getRefreshToken({ _id: req.user._id })

            const token = new RefreshToken({ userId: req.user.id, value: refreshToken })
            await token.save()

            res.statusCode = 200
            res.cookie('accessToken', accessToken, config.cookies)
            res.cookie('refreshToken', refreshToken, config.cookies)
            res.setHeader('Content-Type', 'application/json')
            const userInfo = await User.findById(req.user._id).populate('roleInfoId')
            // const practitionerSpeciality = await Practitioner.find({
            //     // specialty: 'Infermier',
            // }).populate('userId')

            //Envoie d'email de rappel de rdv aux utilisateurs concernées
            await sendMessageReminder()

            res.json({
                success: true,
                status: 'You are successfully logged in!',
                userInfo: userInfo,
                // practitionerSpeciality: practitionerSpeciality,
            })
        } catch (err) {
            next(err)
        }
    },

    register: async function (req, res, next) {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    success: false,
                    errors: errors.array(),
                })
            }
            await User.register(
                new User({ username: req.body.username, email: req.body.email }),
                req.body.password,
                (err, user) => {
                    if (err) {
                        res.statusCode = 500
                        res.setHeader('Content-Type', 'application/json')
                        res.json({ err: err })
                    } else {
                        passport.authenticate('local')(req, res, () => {
                            res.statusCode = 200
                            res.setHeader('Content-Type', 'application/json')
                            res.json({ success: true, status: 'Registration successful!' })
                        })
                    }
                },
            )
        } catch (err) {
            next(err)
        }
    },

    refresh: async function (req, res, next) {
        try {
            let refreshToken = authenticate.jwtRefreshTokenCookieExtractor(req)
            let token = await RefreshToken.findOne({ userId: req.user.id, value: refreshToken })

            if (token && token.revoked) {
                const user = await User.findOne({ _id: req.user.id })
                if (user) {
                    user.revoked = Date.now()
                    await user.save()
                }
                res.status(401).json({
                    success: false,
                    message:
                        "You are using a used JWT token, it's suspicious, your account is banned.",
                })
                return
            }

            if (token && !token.revoked) {
                token.revoked = Date.now()
                await token.save()
            }

            const accessToken = authenticate.getAccessToken({ _id: req.user._id })
            refreshToken = authenticate.getRefreshToken({ _id: req.user._id })

            token = new RefreshToken({ userId: req.user.id, value: refreshToken })
            await token.save()

            res.statusCode = 200
            res.cookie('accessToken', accessToken, config.cookies)
            res.cookie('refreshToken', refreshToken, config.cookies)
            res.setHeader('Content-Type', 'application/json')
            res.json({
                success: true,
                status: 'You have successfully refreshed your JWT access token!',
            })
        } catch (err) {
            next(err)
        }
    },

    revoke: async (req, res, next) => {
        try {
            let refreshToken = authenticate.jwtRefreshTokenCookieExtractor(req)
            let token = await RefreshToken.findOne({ userId: req.user.id, value: refreshToken })

            if (token && token.revoked) {
                const user = await User.findOne({ _id: req.user.id })
                if (user) {
                    user.revoked = Date.now()
                    await user.save()
                }
                res.status(401).json({
                    success: false,
                    message:
                        "You are using a used JWT token, it's suspicious, your account is banned.",
                })
                return
            }

            if (token && !token.revoked) {
                token.revoked = Date.now()
                await token.save()
            }
            res.statusCode = 200
            res.setHeader('Content-Type', 'application/json')
            res.json({
                success: true,
                status: 'You have successfully been logout on the server side!',
            })
        } catch (err) {
            next(err)
        }
    },

    registerPatient: async function (req, res, next) {
        try {
            //   const { password, email, numSecu, lastName, firstName , phoneNumber, address} = req.body;

            const {
                password,
                email,
                numSecu,
                lastName,
                firstName,
                phoneNumber,
                address,
                zipCode,
                city,
            } = req.body

            // Création d'un nouvel utilisateur
            const user = new User({
                username: email,
                email: email,
                roles: 'Patient',
                address: address,
                zipCode: zipCode,
                city: city,
                phoneNumber: phoneNumber,
            })

            // Création d'un nouvel utilisateur
            //   const user = new User({
            //       username: email,
            //       email: email,
            //       roles: 'Patient',
            //       address: address,
            //       phoneNumber: phoneNumber
            //     });

            // Sauvegarde de l'utilisateur dans la base de données
            const savedUser = await User.register(user, password)

            // Création d'un nouveau patient avec la référence vers l'utilisateur
            const patient = new Patient({
                userId: savedUser._id,
                medicalNumber: numSecu,
                firstName: firstName,
                lastName: lastName,
            })

            // Sauvegarde du patient dans la base de données
            const savedPatient = await patient.save()

            // Mise à jour de la référence dans infoSup du User avec l'ID du patient
            savedUser.roleInfoId = savedPatient._id
            await savedUser.save()

            res.status(200).json({
                message: 'Patient enregistré avec succès.',
                userId: savedUser._id,
                patientId: savedPatient._id,
            })
        } catch (err) {
            next(err)
        }
    },

    registerPractitioner: async function (req, res, next) {
        try {
            const {
                password,
                email,
                lastName,
                firstName,
                phoneNumber,
                address,
                zipCode,
                city,
                specialty,
                card,
                institute,
                startTime,
                endTime,
                timeInterval,
            } = req.body

            const { cardNumber, exp_month, exp_year, cvc } = card

            // Création d'un nouvel utilisateur
            const user = new User({
                username: email,
                email: email,
                roles: 'Practitioner',
                address: address,
                zipCode: zipCode,
                city: city,
                phoneNumber: phoneNumber,
            })

            // Sauvegarde de l'utilisateur dans la base de données
            const savedUser = await User.register(user, password)

            // Date actuelle
            var startDate = new Date()
            //startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());

            // Fin de x en cours
            const endDate = day(startDate).endOf('year').toDate()
            const workingHours = {
                startDate: startDate,
                endDate: endDate,
                startTime: startTime,
                endTime: endTime,
                timeInterval: timeInterval,
            }

            // Création d'un nouveau patient avec la référence vers l'utilisateur
            const practitioner = new Practitioner({
                userId: savedUser._id,
                firstName: firstName,
                lastName: lastName,
                specialty: specialty,
                institute: institute,
                workingHours: workingHours,
            })
            practitioner.validateWorkingHours()
            console.log(practitioner)

            // Sauvegarde du patient dans la base de données
            const savedPract = await practitioner.save()

            try {
                const cardToken = paymentMiddleware.generateCardToken(
                    cardNumber,
                    exp_month,
                    exp_year,
                    cvc,
                )

                const customer = await stripe.customers.create({
                    source: cardToken,
                    email: email, // Adresse e-mail du client
                })

                await stripe.customers.createSource(customer.id, { source: 'tok_visa' })

                // Création de la souscription pour le client
                const subscription = await stripe.subscriptions.create({
                    customer: customer.id,
                    items: [{ plan: 'medplane_practitioner_subscription' }],
                })

                // Mise à jour de la référence dans infoSup du User avec l'ID du patient
                savedUser.roleInfoId = savedPract._id
                await savedUser.save()

                res.status(200).json({
                    message: 'Practicien enregistré avec succès.',
                    userId: savedUser._id,
                    practitionerId: savedPract._id,
                    subscriptionId: subscription.id,
                })
            } catch (err) {
                // erreur de paiement
                console.log(err)
                res.status(400).json({
                    message: 'Erreur de paiement. Veuillez vérifier les informations de carte.',
                })
            }
        } catch (err) {
            console.error('Erreur de validation des heures de travail:', err.message)
            next(err)
        }
    },

    registerInstitute: async function (req, res, next) {
        try {
            const { password, email, phoneNumber, address, zipCode, city, name, role } = req.body

            // Création d'un nouvel utilisateur
            const user = new User({
                username: email,
                email: email,
                roles: role, // 'Medical institute' or 'Pharmacy'
                address: address,
                zipCode: zipCode,
                city: city,
                phoneNumber: phoneNumber,
            })

            // Sauvegarde de l'utilisateur dans la base de données
            const savedUser = await User.register(user, password)

            // Création d'un nouvel institut avec la référence vers l'utilisateur
            let institute
            if (role === 'Medical institute') {
                institute = new MedicalInstitute({
                    userId: savedUser._id,
                    name: name,
                })
            } else if (role === 'Pharmacy') {
                institute = new Pharmacy({
                    userId: savedUser._id,
                    name: name,
                })
            }

            // Sauvegarde du patient dans la base de données
            const savedInst = await institute.save()

            // Mise à jour de la référence dans infoSup du User avec l'ID du patient
            savedUser.roleInfoId = savedInst._id
            await savedUser.save()

            res.status(200).json({
                message: role + ' enregistré avec succès.',
                userId: savedUser._id,
                medicalInstituteId: savedInst._id,
            })
        } catch (err) {
            console.log(err)
            next(err)
        }
    },
    requestPasswordReset: async function (req, res, next) {
        //methode post pour l'envoie du token à l'adresse email
        try {
            const { email } = req.body

            /*
          if (!validator.isEmail(email)) {
            return res.status(400).send("L'adresse e-mail est invalide.");
          }
          */

            const user = await User.findOne({ email: email })

            if (!user) {
                //verifie si l'utilisateur existe
                const mess = 'Aucun compte avec cette adresse email'
                console.log(mess)
                return res.status(400).send(mess)
            }
            let resetToken = await ResetToken.findOne({ userId: user._id })
            if (!resetToken) {
                //const token = jwt.sign({ userID: user._id }, accessTokenPrivateKey, { expiresIn: '3m' });
                const resetTokenSign = authenticate.getResetToken({ _id: user._id })
                resetToken = await new ResetToken({
                    userId: user._id,
                    token: resetTokenSign,
                }).save()
            }
            //tokenResetPassword

            //const token = jwt.sign({ userID: user._id }, accessTokenPrivateKey, { expiresIn: '3m' });
            const resetLink = `${process.env.BASE_URL_DEV}/auth/reset-password/${user._id}/${resetToken.token}`

            const subject = 'Password reset'
            const text = `Bonjour,\n\nVous avez demandé à réinitialiser votre mot de passe pour notre application.\n\nPour réinitialiser votre mot de passe, veuillez cliquer sur le lien suivant :\n\n${resetLink} \n\nSi vous n'avez pas demandé à réinitialiser votre mot de passe, veuillez ignorer ce message.\n\nCordialement,\n\nL'équipe medplane`
            await sendEmail(user.email, subject, text)
            res.status(200).json({
                message:
                    'lien de réinitialisation du mot de passe envoyé à votre compte de messagerie',
            })
        } catch (err) {
            console.error(err)
            next(err)
        }
    },
    resetPassword: async function (req, res, next) {
        //methode post pr confimer le nv mdp
        try {
            if (req.body.password !== req.body.confirmPassword) {
                // Si le mot de passe et la confirmation de mot de passe ne correspondent pas, retourne une erreur.
                return res.status(400).send('Les mots de passe ne correspondent pas.')
            }
            const user = await User.findById(req.params.userId)

            if (!user) return res.status(400).send('invalid link or expired')

            const token = await ResetToken.findOne({
                userId: user._id,
                token: req.params.token,
            })
            if (!token) return res.status(400).send('Invalid link or expired')
            const verifyjwt = await jwt.verify(token.token, config.auth.resetTokenPrivateKey)
            await user.setPassword(req.body.password)
            await user.save()
            await token.delete()
            res.status(200).send('password reset sucessfully.')
        } catch (err) {
            if (err.name === 'TokenExpiredError') {
                const mess = 'Le jeton de réinitialisation de mot de passe est expiré'
                console.log(mess)
                res.send(mess)
            }
            console.error(err)
            next(err)
        }
    },
}

module.exports = controller
