const User = require('../models/user.model')
const Practitioner = require('../models/practitioner.model')
const Patient = require('../models/patient.model')
const Appointment = require('../models/appointment.model')
const { compareDates, isWithin24Hours } = require('../utils/utils')

const controller = {
    practitionerAddPlanification: async function (req, res, next) {
        try {
            //const { startDate, endDate, startTime, endTime, timeInterval, practitionerId} = req.body;
            const { startTime, endTime, timeInterval, practitionerId } = req.body
            /*
            const workingHours={
                startDate: new Date('2023-06-05'),
                endDate: new Date('2023-06-10'),
                startTime: '09:00',
                endTime: '18:00',
                timeInterval: timeInterval
              }
            */

            // Date actuelle
            const startDate = Date.now()

            // Fin de x en cours
            const endDate = day(startDate).endOf('year').toDate()
            const workingHours = {
                startDate: startDate,
                endDate: endDate,
                startTime: startTime,
                endTime: endTime,
                timeInterval: timeInterval,
            }

            const practitioner = await Practitioner.findOne({ user: practitionerId })
            practitioner.workingHours = workingHours
            await practitioner.save()

            res.json({
                practitioner: practitioner,
            })
        } catch (err) {
            next(err)
        }
    },
    practionnerAllSlots: async function (req, res, next) {
        //que le medecin
        try {
            //const {  practitionerId} = req.body;
            const practitionerId = req.user.roleInfoId // Supposons que l'ID du médecin connecté est disponible dans req.user.id après l'authentification
            const practitioner = await Practitioner.findById(practitionerId)

            if (!practitioner) {
                return res.status(404).json({ message: 'Médecin introuvable' })
            }
            const allSlots = await practitioner.allSlots

            return res.status(200).json({ mess: req.user, slots: allSlots })
        } catch (err) {
            next(err)
        }
    },
    appointments: async function (req, res, next) {
        try {
            const { date, time, reason, comment } = req.body // patientId, practitionerId,
            var patientId, practitionerId
            if (req.user.roles === 'Practitioner') {
                //si medecin connecter recuperer l'id patient pr le rdv
                if (!req.body.patientId)
                    return res
                        .status(404)
                        .json({ message: `pour le medecin le champ "patientId" est requis` })
                patientId = req.body.patientId
                practitionerId = req.user.roleInfoId //(await Practitioner.findById(req.user.roleInfoId))._id;
            }

            if (req.user.roles === 'Patient') {
                //si patient connecter recuperer l'id medecin pr le rdv
                if (!req.body.practitionerId)
                    return res
                        .status(404)
                        .json({ message: `Pour le patient le champ "practitionerId" est requis` })
                practitionerId = req.body.practitionerId
                patientId = req.user.roleInfoId
            }

            // Rechercher le médecin par son ID
            const practitioner = await Practitioner.findById(practitionerId)

            if (!practitioner) {
                return res.status(404).json({ message: 'Médecin introuvable' })
            }
            // Rechercher le patient par son ID
            const patient = await Patient.findById(patientId)

            if (!patient) {
                return res.status(404).json({ message: 'Patient introuvable' })
            }

            //verifie si la date est > ou = à la date du jour, pour ne pas prendre de rdv anterieur à la date du jours
            const currentDate = new Date()
            const formattedcurrentTime = currentDate.toLocaleTimeString([], {
                hour: '2-digit',
                minute: '2-digit',
            })

            //const resCompareDates=compareDates(currentDate,new Date(date));

            // Convertir la date au format "MM/DD/YYYY" pour la comparaison
            const formattedDate = new Date(date).toLocaleDateString()

            // Convertir la date et l'heure des paramètres en un seul objet de date
            const paramDateTime = new Date(`${new Date(date).toISOString().split('T')[0]}T${time}`)

            if (currentDate >= paramDateTime) {
                //const formattedDate = new Date(date).toLocaleDateString();
                return res
                    .status(404)
                    .json({
                        message: `La date ou l'heure souhaité ne peuvent pas être anterieur à la date du jour`,
                        dateJour: {
                            date: currentDate.toLocaleDateString(),
                            time: formattedcurrentTime,
                        },
                        VosDonnées: { date: formattedDate, time: time },
                    })
            }

            // Vérifier si le créneau est disponible
            var isSlotAvailable = await practitioner.allSlots

            isSlotAvailable = isSlotAvailable.some(
                (slot) => slot.date === formattedDate && slot.slots.includes(time),
            )
            if (!isSlotAvailable) {
                return res
                    .status(400)
                    .json({ message: 'Créneau non disponible', isSlotAvailable: isSlotAvailable })
            }
            // Convertir la date et l'heure en un objet Date
            const appointmentDate = new Date(`${date}T${time}:00.000Z`)

            //verifie si le rdv existe
            /*//inutilise car verification deja fait precedement
            const existAppointment= await Appointment.findOne({
                date:appointmentDate,//new Date(date),
                time:time,//HH:mm
                practitionerId: practitioner._id
            })
            
            if(existAppointment){
                return res.status(400).json({ message: 'Rdv déjà existnt pour ce practicien à cette date',  appointment:existAppointment});
            }
            */

            // Créer le rendez-vous
            const appointment = new Appointment({
                date: appointmentDate,
                time: time, //HH:mm
                patientId: patient._id,
                practitionerId: practitioner._id,
                reason: reason,
                comment: comment,
            })

            //enregistrer le rdv
            await appointment.save()

            return res.status(200).json({ message: 'Rendez-vous pris avec succès' })
        } catch (err) {
            next(err)
        }
    },
    practitionerGetAvailableSlots: async function (req, res, next) {
        try {
            const { practitionerId } = req.body

            const practitioner = await Practitioner.findById(practitionerId)
            if (!practitioner) {
                return res.status(404).json({ message: 'Médecin introuvable' })
            }

            const availableSlots = await practitioner.availableSlotsOnly
            res.json(availableSlots)
        } catch (err) {
            next(err)
        }
    },
    getAppointments: async function (req, res, next) {
        try {
            if (req.user.roles === 'Practitioner') {
                const appointments = await Appointment.find({ practitionerId: req.user.roleInfoId })
                return res.status(200).json(appointments)
            }
            if (req.user.roles === 'Patient') {
                const appointments = await Appointment.find({ patientId: req.user.roleInfoId })
                return res.status(200).json(appointments)
            }

            return res
                .status(400)
                .json(
                    "aucun methode de recuperation de rdv n'a été fait pr votre role: " +
                        req.user.roles +
                        '\n si erreur veuillez contacter un admin',
                )
        } catch (err) {
            next(err)
        }
    },
    deleteAppointment: async function (req, res, next) {
        try {
            const appointmentId = req.params.appointmentId

            // Vérifier si le rendez-vous existe
            const appointment = await Appointment.findById(appointmentId)
            if (!appointment) {
                return res.status(404).json({ message: "Le rendez-vous n'existe pas" })
            }

            if (req.user.roles === 'Practitioner') {
                //si medecin connecter recuperer l'id patient pr le rdv
                const isDoctor = appointment.doctorId.equals(req.user.roleInfoId)
                if (!isDoctor) {
                    return res
                        .status(403)
                        .json({ message: "Vous n'êtes pas autorisé à supprimer ce rendez-vous" })
                }
            }

            if (req.user.roles === 'Patient') {
                //si patient connecter recuperer l'id medecin pr le rdv
                const isPatient = appointment.patientId.equals(req.user.roleInfoId)
                if (!isPatient) {
                    return res
                        .status(403)
                        .json({
                            message: "Vous n'êtes pas autorisé à supprimer ce rendez-vous",
                            user: req.user,
                        })
                }

                // Vérifier si le rendez-vous est deja passé
                const dateNow = new Date()
                const isDeletable = isPatient && dateNow <= appointment.date

                if (!isDeletable) {
                    return res
                        .status(403)
                        .json({
                            message:
                                "La suppression du rendez-vous n'est pas autorisée, date passé",
                        })
                }
            }

            // Utilisation de la méthode deleteOne pour supprimer le rendez-vous
            const result = await Appointment.deleteOne({ _id: appointmentId })

            if (result.deletedCount === 0) {
                // Aucun rendez-vous trouvé avec cet ID
                return res.status(404).json({ message: 'Rendez-vous non trouvé' })
            }

            // Rendez-vous supprimé avec succès
            return res.status(200).json({ message: 'Rendez-vous supprimé avec succès' })
        } catch (err) {
            next(err)
        }
    },
    updateAppointment: async function (req, res, next) {
        try {
            const appointmentId = req.params.appointmentId
            const { date, time, reason, comment } = req.body // patientId, practitionerId,
            var patientId, practitionerId

            // Vérifier si le rendez-vous existe
            const appointment = await Appointment.findById(appointmentId)
            if (!appointment) {
                return res.status(404).json({ message: "Le rendez-vous n'existe pas" })
            }

            if (req.user.roles === 'Practitioner') {
                //si medecin connecter recuperer l'id patient pr le rdv
                const isDoctor = appointment.doctorId.equals(req.user.roleInfoId)
                if (!isDoctor) {
                    return res
                        .status(403)
                        .json({ message: "Vous n'êtes pas autorisé à modifier ce rendez-vous" })
                }
            }

            if (req.user.roles === 'Patient') {
                //si patient connecter recuperer l'id medecin pr le rdv
                const isPatient = appointment.patientId.equals(req.user.roleInfoId)
                if (!isPatient) {
                    return res
                        .status(403)
                        .json({ message: "Vous n'êtes pas autorisé à modifier ce rendez-vous" })
                }

                // Vérifier si le rendez-vous est modifiable dans les 24 heures
                const isModifiable = isPatient && isWithin24Hours(appointment.date)

                if (!isModifiable) {
                    return res
                        .status(403)
                        .json({ message: "La modification du rendez-vous n'est pas autorisée" })
                }
            }

            // Rechercher le médecin par son ID
            const practitioner = await Practitioner.findById(practitionerId)

            if (!practitioner) {
                return res.status(404).json({ message: 'Médecin introuvable' })
            }
            // Rechercher le patient par son ID
            const patient = await Patient.findById(patientId)

            if (!patient) {
                return res.status(404).json({ message: 'Patient introuvable' })
            }

            //verifie si la date est > ou = à la date du jour, pour ne pas prendre de rdv anterieur à la date du jours
            const currentDate = new Date()
            const formattedcurrentTime = currentDate.toLocaleTimeString([], {
                hour: '2-digit',
                minute: '2-digit',
            })

            //const resCompareDates=compareDates(currentDate,new Date(date));

            // Convertir la date au format "MM/DD/YYYY" pour la comparaison
            const formattedDate = new Date(date).toLocaleDateString()

            // Convertir la date et l'heure des paramètres en un seul objet de date
            const paramDateTime = new Date(`${new Date(date).toISOString().split('T')[0]}T${time}`)

            if (currentDate >= paramDateTime) {
                return res
                    .status(404)
                    .json({
                        message: `La date ou l'heure souhaité ne peuvent pas être anterieur à la date du jour`,
                        dateJour: {
                            date: currentDate.toLocaleDateString(),
                            time: formattedcurrentTime,
                        },
                        VosDonnées: { date: formattedDate, time: time },
                    })
            }

            // Vérifier si le créneau est disponible
            var isSlotAvailable = await practitioner.allSlots

            isSlotAvailable = isSlotAvailable.some(
                (slot) => slot.date === formattedDate && slot.slots.includes(time),
            )
            if (!isSlotAvailable) {
                return res
                    .status(400)
                    .json({ message: 'Créneau non disponible', isSlotAvailable: isSlotAvailable })
            }
            // Convertir la date et l'heure en un objet Date
            const appointmentDate = new Date(`${date}T${time}:00.000Z`)

            //verifie si le rdv existe
            /*//inutilise car verification deja fait precedement
            const existAppointment= await Appointment.findOne({
                date:appointmentDate,//new Date(date),
                time:time,//HH:mm
                practitionerId: practitioner._id
            })
            
            if(existAppointment){
                return res.status(400).json({ message: 'Rdv déjà existnt pour ce practicien à cette date',  appointment:existAppointment});
            }
            */

            // Effectuer la mise à jour du rendez-vous dans la base de données
            const updatedAppointment = await Appointment.findByIdAndUpdate(
                appointmentId,
                {
                    date: appointmentDate,
                    time: time, //HH:mm
                    reason: reason,
                    comment: comment,
                },
                { new: true },
            )

            res.json(updatedAppointment)

            return res
                .status(200)
                .json({ message: 'Rendez-vous modifié avec succès', data: updatedAppointment })
        } catch (err) {
            next(err)
        }
    },
}

module.exports = controller
