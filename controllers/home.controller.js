const middleware = require('../middleware/home.middleware')
const Practitioner = require('../models/practitioner.model')
const natural = require('natural')

const controller = {
    searchPractitioner: async function (req, res, next) {
        try {
            const practitioners = await Practitioner.find().populate({ path: 'userId' })

            // Computing scores for each practitioner to sort them in the result
            const scoredPractitioners = practitioners.map((practitioner) => {
                let score = 1

                if (req.body.specialty) {
                    var distance = natural.LevenshteinDistance(
                        req.body.specialty,
                        practitioner.specialty,
                    )
                    var similarite =
                        1 -
                        distance /
                            Math.max(req.body.specialty.length, practitioner.specialty.length)

                    score = similarite * 12

                    distance = natural.LevenshteinDistance(
                        req.body.specialty,
                        practitioner.firstName,
                    )
                    similarite =
                        1 -
                        distance /
                            Math.max(req.body.specialty.length, practitioner.firstName.length)

                    score += similarite * 15

                    distance = natural.LevenshteinDistance(
                        req.body.specialty,
                        practitioner.lastName,
                    )
                    similarite =
                        1 -
                        distance / Math.max(req.body.specialty.length, practitioner.lastName.length)

                    score += similarite * 5
                }

                if (req.body.availability) {
                    if (practitioner.availability === req.body.availability) {
                        score += 1
                    }
                }

                if (req.body.city && practitioner.userId.city) {
                    const distance = natural.LevenshteinDistance(
                        req.body.city,
                        practitioner.userId.city,
                    )
                    const similarite =
                        1 -
                        distance / Math.max(req.body.city.length, practitioner.userId.city.length)
                    score += similarite * 2
                }

                return {
                    practitioner,
                    score,
                }
            })

            // Sorting the practitioners according to the scores
            const sortedPractitioners = scoredPractitioners.sort((a, b) => b.score - a.score)

            res.status(200).json({
                message: 'Recherche effectuée avec succès.',
                practitioners: sortedPractitioners,
            })
        } catch (error) {
            // Gérez les erreurs de manière appropriée
            console.error('Erreur lors de la recherche de médecins :', error)
            return { error: "Une erreur s'est produite lors de la recherche de médecins." }
        }
    },
}

module.exports = controller
