<img src="./public/banner.svg" alt=“banner” width="100%">

# Medplane backend

Medplane est une application simple de prise de rendez-vous médicaux.

## Sommaire

- [Medplane ackend](#medplane-ackend)
  - [Sommaire](#sommaire)
  - [Pré-requis](#pré-requis)
  - [Installation](#installation)
  - [CI/CD](#cicd)
  - [Stack Technique](#stack-technique)
    - [Backend](#backend)
    - [Base de données](#base-de-données)
    - [Serveur](#serveur)
      - [Securité](#securité)
    - [Containerization](#containerization)
  - [Fonctionnalités](#fonctionnalités)
  - [Licence](#licence)

## Pré-requis

Pour pouvoir lancer la solution backend il va simplement falloir :

-   NodeJS
-   ExpressJS
-   MongoDB

## Installation

-   Cloner le repository

```bash
git clone https://gitlab.com/medplane/medplane-backend <project_name>
```

-   Créer un fichier d'environnement `.env`

```bash
DB_PORT=27017
DB_NAME=mydb
DB_USER=myuser
DB_PASSWORD=mypassword
ROOT_DB_USERNAME=
ROOT_DB_PASSWORD=
MONGO_URL_LOCAL=mongodb://127.0.0.1:27017/medplane
MONGO_URL_PROD=

# Authentication key pair used for JWT tokens (use a random base64 string generator to make these keys).
ACCESS_TOKEN_PRIVATE_KEY=
REFRESH_TOKEN_PRIVATE_KEY=
RESET_TOKEN_PRIVATE_KEY=

STRIPE_SECRET_KEY=

#email
EMAIL_HOST = # email host
EMAIL_USER = # email id
EMAIL_USER_HEADER =  # email id
EMAIL_PASS = # email password
EMAIL_SERVICE = gmail # email service
BASE_URL_DEV = http://localhost:3000/api
BASE_URL_PROD =
```

-   Lancer le projet via docker-compose

```bash
docker-compose up --force-recreate --build
```

## CI/CD

Le backend utilise les outils d'intégration continue proposé par GitLab.

-   [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
-   [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
-   [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

## Stack Technique

### Backend

-   Node.js
-   Express.js

### Base de données

-   MongoDB
-   MongoDB Atlas

### Serveur

-   AWS EC2

#### Securité

-   HTTPS
-   Passport.js (JWT)
-   Helmet
-   CORS
-   Rate Limit
-   SAST

### Containerization

-   Docker
-   Docker-Compose

## Fonctionnalités

-   Authentification des utilisateurs
-   Recherche de médecins et spécialistes
-   Prise de rendez-vous en ligne
-   Rappels et notifications
-   Gestion des rendez-vous
-   Affichage du profil utilisateur
-   Gestion des dossiers
-   Paiement en ligne

## Licence

-   MIT Licence
