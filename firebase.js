const firebase = require('firebase/app')
// require('firebase/auth');
require('firebase/storage')

const firebaseConfig = {
    apiKey: 'AIzaSyArhXN_cvzENP1vtFfc-THPeXDjO8JXGrA',
    authDomain: 'winter-jet-390115.firebaseapp.com',
    projectId: 'winter-jet-390115',
    storageBucket: 'winter-jet-390115.appspot.com',
    messagingSenderId: '98200975776',
    appId: '1:98200975776:web:97030ae4b3d0a7724e0cc5',
    measurementId: 'G-TSE9RF9PD8',
}

firebase.initializeApp(firebaseConfig)

// const auth = firebase.auth();
const storage = firebase.storage()

module.exports = { storage }
