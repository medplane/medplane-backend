const mongoose = require('mongoose')

const patientSchema = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        firstName: {
            type: String,
            required: true,
        },
        lastName: {
            type: String,
            required: true,
        },
    },
    { timestamps: true },
)

const Patient = mongoose.model('Patient', patientSchema)

module.exports = Patient
