const mongoose = require('mongoose')

const pharmacySchema = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        name: {
            type: String,
            required: true,
        },
    },
    { timestamps: true },
)

const Pharmacy = mongoose.model('Pharmacy', pharmacySchema)

module.exports = Pharmacy
