const mongoose = require('mongoose')

const medicalInstituteSchema = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        name: {
            type: String,
            required: true,
        },
        practitioners: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Practitioner',
                default: [],
            },
        ],
        // Autres propriétés spécifiques au modèle InstitutMedical...
    },
    { timestamps: true },
)

const MedicalInstitute = mongoose.model('MedicalInstitute', medicalInstituteSchema)

module.exports = MedicalInstitute
