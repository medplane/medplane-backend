const mongoose = require('mongoose')
const expireIn = require('../config').auth.expireIn

const resetTokenSchema = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'user',
        },
        token: {
            type: String,
            required: true,
        },
        createdAt: {
            type: Date,
            default: Date.now,
            expires: parseDuration(expireIn.resetToken), // this is the expiry time in seconds (300s=5m)
        },
    },
    {
        collection: 'resetTokens',
        timestamps: true,
    },
)

// Fonction pour convertir la durée en secondes
function parseDuration(durationString) {
    const durationRegex = /^(\d+)([smh])$/
    const matches = durationString.match(durationRegex)

    if (!matches) {
        throw new Error('Invalid duration format')
    }

    const value = parseInt(matches[1], 10)
    const unit = matches[2]

    switch (unit) {
        case 's':
            return value
        case 'm':
            return value * 60
        case 'h':
            return value * 60 * 60
        default:
            throw new Error('Invalid duration unit')
    }
}
module.exports = mongoose.model('resetToken', resetTokenSchema)
