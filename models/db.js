const mongoose = require('mongoose')
const config = require('../config')

const { host, port, name, user, password } = config.db

//const url="mongodb://127.0.0.1:27017/medplane";//mongodb://localhost:27017
const url = config.mongoURL[process.NODE_ENV || 'local'] //prend

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    //bufferCommands: false,
}

mongoose.set('strictQuery', true)
mongoose.connect(url, options)

mongoose.connection.on('connecting', () => {
    console.log('Connecting')
})

mongoose.connection.on('error', () => {
    console.log('Connection error')
})

mongoose.connection.on('connected', () => {
    console.log('Connected to database successfully established')
})

mongoose.connection.on('diconnected', () => {
    console.log('Disconnected')
})

mongoose.connection.on('reconnected', () => {
    console.log('Reconnected')
})

module.exports = mongoose.connection
