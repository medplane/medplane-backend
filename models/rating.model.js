const mongoose = require('mongoose')

const ratingSchema = new mongoose.Schema(
    {
        ratedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Patient',
            required: true,
        },
        ratedEntity: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        entityType: {
            type: String,
            enum: ['Practitioner', 'MedicalInstitute', 'Pharmacy'],
            required: true,
        },
        rating: {
            type: Number,
            min: 1,
            max: 5,
            required: true,
        },
        comment: {
            type: String,
            required: true,
        },
    },
    { timestamps: true },
)

const Rating = mongoose.model('Rating', ratingSchema)

module.exports = Rating
