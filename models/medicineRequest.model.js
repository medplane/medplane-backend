const mongoose = require('mongoose')

const medicineRequestSchema = new mongoose.Schema(
    {
        date: {
            type: Date,
            required: true,
        },
        askerId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        askerRole: {
            type: String,
        },
        pharmacyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Pharmacy',
            required: true,
        },
        medicine: {
            // the medicine the asker looks for
            type: String,
            required: true,
        },
        status: {
            type: String,
            enum: ['Demandé', 'Disponible', 'Indisponible','N/A'],
            default: 'Demandé',
        },
        comments: [
            {
                commentedBy: {
                    type: mongoose.Schema.Types.ObjectId,
                    required: true,
                },
                date: {
                    type: Date,
                    required: true,
                },
                message: {
                    type: String,
                    required: true,
                },
            },
        ],
        updatedAt: {
            type: Date,
            required: false,
        },
    },
    { timestamps: true },
)

const MedicineRequest = mongoose.model('MedicineRequest', medicineRequestSchema)

module.exports = MedicineRequest
