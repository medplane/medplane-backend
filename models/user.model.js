const mongoose = require('mongoose')
const passportLocalMongoose = require('passport-local-mongoose')
const roles = require('../config').roles
const mongooseTypeEmail = require('mongoose-type-email')

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       required:
 *         - username
 *         - password
 *         - email
 *       properties:
 *         _id:
 *           type: string
 *           description: The auto-generated id of the user
 *         username:
 *           type: string
 *           description: User username
 *         password:
 *           type: string
 *           description: User password
 *         email:
 *           type: string
 *           description: User email
 *         roles:
 *           type: array
 *           description: Roles of the user
 *         revoked:
 *            type: string
 *            format: date
 *            description: Date when this user got banned
 *         createdAt:
 *           type: string
 *           format: date
 *           description: The date the user was added
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: The last date when the user was updated
 */
const userSchema = new mongoose.Schema(
    {
        username: {
            type: String,
        },
        address: {
            type: String, // address-parser
            required: true,
        },
        zipCode: {
            type: Number,
            required: true,
        },
        city: {
            type: String,
            required: true,
        },
        phoneNumber: {
            type: Number,
            required: true,
            unique: true,
        },
        email: {
            type: mongooseTypeEmail,
            required: true,
            unique: true,
        },
        password: {
            type: String,
        },
        roles: {
            type: String,
            enum: ['Patient', 'Practitioner', 'Medical institute', 'Pharmacy', 'Admin'],
            default: 'Patient',
        },
        roleInfoId: {
            //infoSupId
            type: mongoose.Schema.Types.ObjectId,
            refPath: 'roles',
        },
        revoked: {
            type: Date,
            default: null,
        },
    },
    { timestamps: true },
)

// Utilise l'email comme nom d'utilisateur (username) pour l'authentification
userSchema.plugin(passportLocalMongoose, {
    usernameField: 'email',
})

const User = mongoose.model('User', userSchema)

module.exports = User
