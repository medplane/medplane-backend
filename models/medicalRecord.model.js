const mongoose = require('mongoose')

const medicalRecordSchema = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        patientId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Patient',
            required: true,
        },
        fileName: {
            type: String,
            required: true,
        },
        fileUrl: {
            type: String,
            required: true,
        },
        uploadedDate: {
            type: Date,
        },
    },
    { timestamps: true },
)

const MedicalRecord = mongoose.model('MedicalRecord', medicalRecordSchema)

module.exports = MedicalRecord
