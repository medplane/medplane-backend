const mongoose = require('mongoose')

const appointmentSchema = new mongoose.Schema(
    {
        date: {
            type: Date,
            required: true,
        },
        time: {
            type: String,
            require: true,
        },
        patientId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Patient',
            required: true,
        },
        practitionerId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Practitioner',
            required: true,
        },
        reason: {
            //laisser la saisi au patient ou imposer
            type: String,
            required: true,
        },
        comment: {
            type: String,
            required: false,
        },
        reminderSent24h: {
            type: Boolean,
            default: false,
        },
    },
    { timestamps: true },
)

const Appointment = mongoose.model('Appointment', appointmentSchema)

module.exports = Appointment
