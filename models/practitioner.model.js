const mongoose = require('mongoose')
const Appointment = require('../models/appointment.model')
const { getStartAndEndOfDay, compareDates } = require('../utils/utils')

const practitionerSchema = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        firstName: {
            type: String,
            required: true,
        },
        lastName: {
            type: String,
            required: true,
        },
        specialty: {
            type: String,
            required: true,
        },
        availability: {
            type: String,
            //required: true,
        },
        institute: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'MedicalInstitute',
            required: false,
        },
        workingHours: {
            startDate: { type: Date, required: true }, // Date de début
            endDate: { type: Date, required: true }, // Date de fin
            startTime: { type: String, required: true }, // Heure de début de travail (par exemple : "09:00")
            endTime: { type: String, required: true }, // Heure de fin de travail (par exemple : "17:00")
            timeInterval: { type: Number, required: true }, // Intervalle de temps entre chaque créneau horaire (en minutes)
        },
    },
    { timestamps: true },
)

// Méthode de validation pour le schéma WorkingHours
practitionerSchema.methods.validateWorkingHours = function () {
    // Vérification de la date de fin supérieure à la date de début
    const { startDate, endDate, startTime, endTime, timeInterval } = this.workingHours

    if (endDate < startDate) {
        throw new Error('La date de fin doit être postérieure à la date de début.')
    }

    // Vérification de l'heure de fin supérieure à l'heure de début
    if (endTime <= startTime) {
        throw new Error("L'heure de fin doit être postérieure à l'heure de début.")
    }

    // Calcul de la durée totale en minutes
    const startMinutes = Number(startTime.split(':')[0]) * 60 + Number(startTime.split(':')[1])
    const endMinutes = Number(endTime.split(':')[0]) * 60 + Number(endTime.split(':')[1])
    const totalMinutes = endMinutes - startMinutes

    // Vérification de l'écart d'au moins timeInterval avec %
    if (totalMinutes % timeInterval !== 0) {
        throw new Error(
            `L'écart entre l'heure de début et l'heure de fin doit être un multiple de ${timeInterval} minutes.`,
        )
    }
}

practitionerSchema.virtual('allSlots').get(async function () {
    //availableSlots
    const { startDate, endDate, startTime, endTime, timeInterval } = this.workingHours
    const slots = []
    const currentDate = new Date(startDate)
    const endDateObj = new Date(endDate)

    // Parcourir chaque jour dans l'intervalle de dates
    while (currentDate <= endDateObj) {
        const currentDay = {
            date: currentDate.toLocaleDateString(),
            slots: [],
        }

        const currentDayStartTime = new Date(
            currentDate.getFullYear(),
            currentDate.getMonth(),
            currentDate.getDate(),
            parseInt(startTime.split(':')[0]),
            parseInt(startTime.split(':')[1]),
        )
        const currentDayEndTime = new Date(
            currentDate.getFullYear(),
            currentDate.getMonth(),
            currentDate.getDate(),
            parseInt(endTime.split(':')[0]),
            parseInt(endTime.split(':')[1]),
        )

        // Générer les créneaux horaires pour le jour courant
        let currentTime = currentDayStartTime
        //console.log("currentDayStartTime: "+currentDayEndTime)
        //console.log("currentDayStartTime: "+currentDayStartTime+"  currentDayEndTime: "+currentDayEndTime)
        while (currentTime <= currentDayEndTime) {
            currentDay.slots.push(
                currentTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' }),
            )
            currentTime.setMinutes(currentTime.getMinutes() + timeInterval)
            //console.log("currentDayStartTime: "+currentDayStartTime)
        }
        //console.log(currentDay)
        slots.push(currentDay)
        currentDate.setDate(currentDate.getDate() + 1)
    }
    //console.log({here:slots})
    return slots
})

practitionerSchema.virtual('availableSlotsOnly').get(async function () {
    /*
    const doctor = await Practitioner.findById(practitionerId);
    if (!doctor) {
      throw new Error('Médecin introuvable.');
    }
    */

    const { startDate, endDate, startTime, endTime, timeInterval } = this.workingHours
    try {
        const slots = []

        // Parcourir chaque jour dans l'intervalle de dates
        let currentDate = new Date(startDate)
        const endDateObj = new Date(endDate)

        while (currentDate <= endDateObj) {
            const currentDateToday = new Date()
            //const formattedcurrentTime=currentDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });

            const resCompareDates = compareDates(currentDateToday, currentDate)
            //console.log(resCompareDates)

            if (resCompareDates === 1) {
                currentDate.setDate(currentDate.getDate() + 1)
                continue
            }

            const currentDay = {
                date: currentDate.toLocaleDateString(),
                slots: [],
            }

            const currentDayStartTime = new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                currentDate.getDate(),
                parseInt(startTime.split(':')[0]),
                parseInt(startTime.split(':')[1]),
            )
            const currentDayEndTime = new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                currentDate.getDate(),
                parseInt(endTime.split(':')[0]),
                parseInt(endTime.split(':')[1]),
            )

            // Générer les créneaux horaires pour le jour courant
            let currentTime = currentDayStartTime
            const { startOfDay, endOfDay } = getStartAndEndOfDay(currentDate)

            while (currentTime <= currentDayEndTime) {
                const isSlotAvailable = await Appointment.findOne({
                    practitionerId: this._id,
                    date: {
                        //comparaison basée uniquement sur l'année, le mois et le jour :
                        $gte: startOfDay, //$gte pour vérifier si la date est supérieure ou égale à startOfDay,
                        $lt: endOfDay, //lt pour vérifier si la date est inférieure à endOfDay
                    },
                    time: currentTime.toLocaleTimeString([], {
                        hour: '2-digit',
                        minute: '2-digit',
                    }),
                })

                /*
        if(resCompareDates === 1 && formattedcurrentTime >= time) {
          //const formattedDate = new Date(date).toLocaleDateString();
          return res.status(404).json({ message: `La date ou l'heure souhaité ne peuvent pas être anterieur à la date du jour`, 
                                      dateJour:{date:currentDate.toLocaleDateString(),time:formattedcurrentTime}, 
                                      VosDonnées:{date:formattedDate,time:time}} );
      }
      */
                if (!isSlotAvailable) {
                    currentDay.slots.push(
                        currentTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' }),
                    )
                }
                currentTime.setMinutes(currentTime.getMinutes() + timeInterval)
            }

            slots.push(currentDay)
            currentDate.setDate(currentDate.getDate() + 1)
        }
        //console.log(slots)
        return slots
    } catch (err) {
        throw err
    }
})

const Practitioner = mongoose.model('Practitioner', practitionerSchema)

module.exports = Practitioner
